package nl.praegus;

public class Whale extends Mammal {
    public Whale(String name, int temperature) {
        super(name, temperature);
    }
}
