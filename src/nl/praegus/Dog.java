package nl.praegus;

public class Dog extends Mammal {
    private int numberOfLegs;
    private String[] dailyRhythm = {"poops", "sleeps", "eats", "walks"};

    private static int maximumNumberOfLegs = 4;

    public Dog(String name, int temperature, int numberOfLegs) {
        super(name, temperature);
        this.numberOfLegs = numberOfLegs;
    }

    public void amputateLeg() {
        numberOfLegs = numberOfLegs - 1;
    }

    public boolean numberOfLegsIsEven() {
        return numberOfLegs == 0 || numberOfLegs == 2 || numberOfLegs == 4;
    }

    public boolean isSick() {
        if (getTemperature() < 39 || numberOfLegs < 4) {
            return true;
        } else {
            return false;
        }
    }

    public boolean needsRoboticLeg() {
        switch (numberOfLegs) {
            case 0:
            case 1:
            case 2:
            case 3:
                return true;
            default:
                return false;
        }
    }

    public void printRhythmWhile() {
        int counter = 0;
        while (counter < dailyRhythm.length) {
            System.out.println("The dog " + dailyRhythm[counter]);
        }
    }

    public void printRhythmFor() {
        for (int counter = 0; counter < dailyRhythm.length; counter++) {
            System.out.println("The dog " + dailyRhythm[counter]);
        }
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    public static int getMaximumNumberOfLegs() {
        return maximumNumberOfLegs;
    }
}
